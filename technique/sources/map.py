from dataclasses import dataclass
import pygame, pytmx, pyscroll
from player import NPC
@dataclass
class Portal:
    from_world: str
    origin_point: str
    target_world: str
    teleport_point: str
    active: bool
@dataclass
class Map:
    name: str
    walls: list[pygame.Rect]
    group: pyscroll.PyscrollGroup
    tmx_data: pytmx.TiledMap
    portals: list[Portal]
    npcs: list[NPC]

class MapManager:
    def __init__(self, screen, player):
        self.maps = dict() #exemple: "lvl2" -> Map("lvl2", walls, group)
        self.screen = screen
        self.player = player
        self.current_map = "carte"
        self.active_portal = None

        self.register_map("carte", portals=[
            Portal(from_world="carte", origin_point="enter_lvl2", target_world="carte2", teleport_point="lvl2", active = False)
        ], npcs=[
            NPC("cyberion", nb_points=4, dialog=["Bienvenue, mon nom est Cyberion, je suis ton guide. [...]",
                                                 "Ta machine a été infectée. [...]",
                                                 "Pour récupérer tes données chiffrées,"
                                                 " tu dois compléter ce jeu. [...]",
                                                 "Ceci est le premier niveau. [...]",
                                                 "Rends-toi sur https://www.challenges-kids.fr/categories/web/chall2/ [...]",
                                                 "Une fois sur le site,"
                                                 " trouve le code secret et remets-le à la CyberSentinelle bleue. [...]",
                                                 "Elle garde la porte de transition vers le niveau 2. [...]",
                                                 "Attention: le code secret ne doit pas contenir de caractères spéciaux, [...]",
                                                 " ni d'espaces. Enfin, [...]",
                                                 "Lorsque tu insères un code, vérifie bien que tu commences au début de la boîte de sasie."
                                                 ])
        ])

        self.register_map("carte2", portals=[
            Portal(from_world="carte2", origin_point="enter_lvl3", target_world="carte3", teleport_point="lvl3", active = False)
        ], npcs=[
            NPC("alchemist", nb_points=4, dialog=["AH, aventurier! [...]",
                                                  "Ton arrivée est une lueur d'espoir dans cette longue attente de trois siècles [...]",
                                                  "J'ai égaré mes précieuses potions dans ce dédale mystique [...]",
                                                  "et aucun joueur n'a réussi à les retrouver jusqu'à présent [...]",
                                                  "Mon espoir en mon but absolu de concevoir un système informatique infaillible s'amenuise [...]",
                                                  "et la folie me guette [...]",
                                                  "Mes potions, vestiges d'un temps oublié, dégagent une énergie magique distinctive. [...]",
                                                  "Elles représentent les clés de la cybersécurité, [...]",
                                                  "gardiennes des secrets contre les menaces numériques. [...]",
                                                  "La première, teintée d'une lueur rubis, représente la vaillance numérique. [...]",
                                                  "La seconde potion, d'une teinte bleue, évoque la sagesse et la connaissance numérique. [...]",
                                                  "Elles sont protégées par des coffres magiques, [...]",
                                                  "Chaque coffre est doublement sécurisé, [...]",
                                                  "nécessitant à la fois une clé et un code pour être ouvert [...]",
                                                  "Ces quatre objets mystiques sont disséminés à travers ce labyrinthe. [...]",

                                                  "Pour me rapporter les potions, tu devras débuter par la potion rouge [...]",
                                                  "Commence par dénicher la clé qui correspond à la potion rouge, [...]",
                                                  "Ensuite, localise le coffre associé. [...]",
                                                  "Réitère ces étapes pour la potion bleue [...]",
                                                  "N'omet pas d'appuyer sur la touche espace à la découverte de chaque objet mystique. [...]",
                                                  "Une fois le coffre bleu ouvert, le portail légendaire derrière moi s'ouvrira. [...]",
                                                  "Une ultime mission, dont je ne suis pas informé, t'attendra. [...]",
                                                  "Achève-la, et ainsi, mon objectif s'accomplira."])
        ])
        self.register_map("carte3", portals=[], npcs=[
            NPC("robot", nb_points=2, dialog=["Pas mal pour un mortel. [...]",
                                              "Cependant, comme l'alchémiste a dû de te le dire, [...]",
                                              "Ce n'est pas encore fini. [...]",
                                              "Bienvenue dans la salle des serveurs. [...]",
                                              "Elle se trouve en dessous de nous, [...]",
                                              "Pour y entrer, la légende raconte une faille où B et C convergent. [...]",
                                              "Une fois entré(e), tu verras 5 serveurs. [...]",
                                              "Ceux qui n'ont pas de bande colorée sont fixes et connectés. [...]",
                                              "Il te faudra visualiser une dimension où, [...]",
                                              "par le biais de pivotements et de déplacements des serveurs à bande colorée, [...]",
                                              "tous les serveurs de cette salle mystique forment un réseau connecté. [...]",
                                              "Dès lors, il faudra remettre au gardien les coordonnées d'un serveur à bande colorée [...]",
                                              "Etant donné que tes données se trouvent dans un des trois serveurs à bande colorée, [...]",
                                              "Un seul triplet de la forme xny fonctionnera, [...]",
                                              "(avec x dans {a,b,c,d}, n dans [1;4] et y dans {o,r,v})."
                                              ])

        ])
        self.teleport_player("player")
        self.teleport_npcs()

    def check_npc_collisions(self, dialog_box):
        for sprite in self.get_group().sprites():
            if sprite.feet.colliderect(self.player.rect) and type(sprite) is NPC:
                dialog_box.execute(sprite.dialog)

    def activate_portal(self, origin_point):
        for portal in self.get_map().portals:
            if portal.from_world == self.current_map and portal.origin_point == origin_point:
                portal.active = True
                self.active_portal = portal
    def check_collisions(self):
        #portails
        for portal in self.get_map().portals:
            if portal.from_world == self.current_map and portal.active:
                point = self.get_object(portal.origin_point)
                rect = pygame.Rect(point.x, point.y, point.width, point.height)
                if self.player.feet.colliderect(rect):
                    copy_portal = portal
                    self.current_map = self.active_portal.target_world
                    self.teleport_player(self.active_portal.teleport_point)
        #boites de sasie du lvl1 et 3
        self.active = False
        if self.current_map == "carte":
            answer_code_rect = self.get_object("answer_code")
            if self.player.feet.colliderect(pygame.Rect(answer_code_rect.x, answer_code_rect.y, answer_code_rect.width, answer_code_rect.height)):
                self.active = True
        if self.current_map == "carte3":
            answer_code_rect2 = self.get_object("answer_code2")
            if self.player.feet.colliderect(pygame.Rect(answer_code_rect2.x, answer_code_rect2.y, answer_code_rect2.width, answer_code_rect2.height)):
                self.active = True

        #collision
        for sprite in self.get_group().sprites():
            if type(sprite) is NPC:
                if sprite.feet.colliderect(self.player.rect):
                    sprite.speed = 0
                else:
                    sprite.speed = 1
            if sprite.feet.collidelist(self.get_walls()) > -1:
                sprite.move_back()

    def teleport_player(self, name):
        point = self.get_object(name)
        self.player.position[0] = point.x
        self.player.position[1] = point.y
        self.player.save_location()
    def register_map(self, name, portals=[], npcs=[]):
        # map (tmx)
        tmx_data = pytmx.util_pygame.load_pygame(f"{name}.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        map_layer.zoom = 3
        # liste stockant les rectangles de collision
        walls = []
        for obj in tmx_data.objects:
            if obj.type == "collision":
                walls.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # dessiner le groupe de calques
        group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=3)
        group.add(self.player)

        #récupérer tous les npcs pour les ajouter au groupe
        for npc in npcs:
            group.add(npc)

        #enregistrer la nouvelle carte chargée
        self.maps[name] = Map(name, walls, group, tmx_data, portals, npcs)

    def get_map(self): return self.maps[self.current_map]

    def get_group(self): return self.get_map().group

    def get_walls(self): return self.get_map().walls

    def get_object(self, name):
        map_data = self.get_map().tmx_data
        if name in map_data.objects_by_name:
            return map_data.objects_by_name[name]

    def teleport_npcs(self):
        for map in self.maps:
            map_data = self.maps[map]
            npcs = map_data.npcs
            for npc in npcs:
                npc.load_points(map_data.tmx_data)
                npc.teleport_spawn()

    def draw(self):
        self.get_group().draw(self.screen)
        self.get_group().center(self.player.rect.center)

    def update(self):
        self.get_group().update()
        self.check_collisions()
        for npc in self.get_map().npcs:
            npc.move()