import pygame
import game


class Level2:
    def __init__(self, dialog_box, map_manager, input_box, game_instance):
        self.game = game_instance
        self.dialog_box = dialog_box
        self.map_manager = map_manager
        self.input_box = input_box
        self.current_color = 'rouge'
        self.key_chest_pairs = {
            'rouge': ('red_key', 'red_chest'),
            'bleue': ('blue_key', 'blue_chest'),
        }
        self.exit_door_opened = False
        self.found_keys = {
            'rouge': False,
            'bleue': False,
        }
    def player_interacts_with_object(self, object_type):
        key, chest = self.key_chest_pairs[self.current_color]
        if object_type == key:
            self.dialog_box.execute([f"Tu as trouvé la clé {self.current_color}."])
            self.update_found_keys(self.current_color)
        elif object_type == chest:
            # vérifie si la clé actuelle a été trouvée
            if self.current_color == 'rouge' and self.found_keys['rouge']:
                self.dialog_box.execute(["Au coeur des arcanes cryptiques, la potion rouge révèle sa vérité en 13 rotations. [...]",
                                         "Aventure-toi dans le rouge des caractères pour dévoiler la clarté du 'zrffntr'."])
                answer_code_rect2 = self.map_manager.get_object("red_chest")
                if self.map_manager.player.feet.colliderect(
                        pygame.Rect(answer_code_rect2.x, answer_code_rect2.y, answer_code_rect2.width,
                                    answer_code_rect2.height)):
                    self.input_box.active = True
                    self.input_box.handle_event()
                    self.game.handle_code_entry2()
            elif self.current_color == 'bleue' and self.found_keys['bleue']:
                self.dialog_box.execute([
                    "Dans l'azur des codes, le secret se dévoile en 64 nuances. [...]",
                    "Explore le bleu des symboles pour percer le mystère du 'message'."
                ])
                self.input_box.active = True
                self.game.handle_code_entry3()
                self.exit_door_opened = True
            else:
                self.dialog_box.execute(["Tu ne peux pas encore ouvrir ce coffre. Trouve d'abord la bonne clé."])
                return  #on sort si la clé correspondante n'a pas été trouvée

    def check_lv2_collisions(self):
        player_rect = self.map_manager.player.feet

        #vérification de la collision avec la clé rouge
        red_key_object = self.map_manager.get_object("red_key")
        if red_key_object and player_rect.colliderect(
                pygame.Rect(red_key_object.x, red_key_object.y, red_key_object.width, red_key_object.height)):
            self.player_interacts_with_object("red_key")

        #vérification de la collision avec le coffre rouge
        red_chest_object = self.map_manager.get_object("red_chest")
        if red_chest_object and player_rect.colliderect(
                pygame.Rect(red_chest_object.x, red_chest_object.y, red_chest_object.width, red_chest_object.height)):
            self.player_interacts_with_object("red_chest")

        #vérification avec la clé bleue
        blue_key_object = self.map_manager.get_object("blue_key")
        if blue_key_object and player_rect.colliderect(
                pygame.Rect(blue_key_object.x, blue_key_object.y, blue_key_object.width, blue_key_object.height)):
            self.player_interacts_with_object("blue_key")

        #vérification de la collision avec le coffre bleu
        blue_chest_object = self.map_manager.get_object("blue_chest")
        if blue_chest_object and player_rect.colliderect(
                pygame.Rect(blue_chest_object.x, blue_chest_object.y, blue_chest_object.width,
                            blue_chest_object.height)):
            self.player_interacts_with_object("blue_chest")
    def update_current_state(self):
        #passer à la prochaine étape de la séquence
        if self.current_color == 'rouge':
            self.current_color = 'bleue'

    def is_exit_door_opened(self):
        return self.exit_door_opened

    def update_found_keys(self, key_color):
        self.found_keys[key_color] = True
