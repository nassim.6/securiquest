import pygame
import pytmx
import pyscroll
import input_box
from dialog import DialogBox
from map import MapManager, Portal
from player import Player
from level2 import Level2
class Game:
    def __init__(self):
        # fenetre du jeu
        self.screen = pygame.display.set_mode((800, 600))
        pygame.display.set_caption("SécuriQuest - Evasion Virtuelle")
        #generer un joueur
        self.player = Player()
        self.map_manager = MapManager(self.screen, self.player)
        self.dialog_box = DialogBox()
        self.base_font = pygame.font.Font(None, 32)
        self.user_text = ''
        self.input_rect = pygame.Rect(200, 200, 140, 32)
        self.color_active = pygame.Color('lightskyblue3')
        self.color_passive = pygame.Color('darkorchid4')
        self.color = self.color_passive
        self.correct_code1 = "htmllangageduweb"
        self.correct_code2 = "message"
        self.correct_code3 = "bwvzc2fnzq=="
        self.correct_code4 = "c2r"
        self.input_box = input_box.InputBox()
        self.level_2 = Level2(self.dialog_box, self.map_manager, self.input_box, self)
        self.remaining_time = 900
        self.elapsed_time = 0
        self.winner = False

    def update_remaining_time(self):
        #update le temps restant en secondes
        current_time = pygame.time.get_ticks() // 1000  #convertir le temps en millisecondes en secondes
        elapsed_since_last_update = current_time - self.elapsed_time
        self.elapsed_time = current_time
        self.remaining_time = max(0, self.remaining_time - elapsed_since_last_update)

    def display_timer(self):
        timer_font = pygame.font.Font(None, 36)
        timer_text = timer_font.render(f"Temps restant : {self.remaining_time}", True, (255, 255, 255))
        self.screen.blit(timer_text, (10, 10))

    def display_tips(self):
        tips_font = pygame.font.Font(None, 22)
        tips_string = "Veillez à bien interagir avec les PNJ en appuyant sur espace après être entré en collision avec eux!"
        words = tips_string.split()
        lines = [" ".join(words[i:i + 4]) for i in range(0, len(words), 4)]
        tips_texts = [tips_font.render(line, True, (255, 255, 255)) for line in lines]

        screen_width, screen_height = self.screen.get_size()
        y_pos = 10  # 10 pixels de marge en haut

        for i, tips_text in enumerate(tips_texts):
            text_width, text_height = tips_text.get_size()
            x_pos = screen_width - text_width - 10  # 10 pixels de marge à droite
            self.screen.blit(tips_text, (x_pos, y_pos + i * text_height))
    def handle_code_entry1(self):
        if self.user_text.lower() == self.correct_code1:
            self.map_manager.activate_portal("enter_lvl2")
            self.user_text = ''

    def handle_code_entry2(self):
        if self.user_text.lower() == self.correct_code2:
            self.level_2.update_current_state()
            self.input_box.active = False
            self.user_text = ''

    def handle_code_entry3(self):
        if self.user_text.lower() == self.correct_code3:
            self.level_2.map_manager.activate_portal("enter_lvl3")
            self.input_box.active = False
            self.user_text = ''

    def handle_code_entry4(self):
        if self.user_text.lower() == self.correct_code4:
            self.user_text = ''
            self.winner = True


    def handle_level_2_interactions(self, object_type):
        self.level_2.player_interacts_with_object(object_type)

    def mouvements(self):
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_UP]:
            self.player.move_up()
        elif pressed[pygame.K_DOWN]:
            self.player.move_down()
        elif pressed[pygame.K_LEFT]:
            self.player.move_left()
        elif pressed[pygame.K_RIGHT]:
            self.player.move_right()

    def update(self):
        self.map_manager.update()

    def run(self):
        clock = pygame.time.Clock()
        # boucle du jeu pour maintenir en activité la fenêtre
        running = True
        events = pygame.event.get()
        while running:
            self.player.save_location()
            self.mouvements()
            self.update()
            self.map_manager.draw()
            self.dialog_box.render(self.screen)
            self.text_surface = self.base_font.render(self.user_text, True, (255, 255, 255))
            self.screen.blit(self.text_surface, (self.input_rect.x + 5, self.input_rect.y + 5))
            self.input_rect.w = max(100, self.text_surface.get_width() + 10)
            self.update_remaining_time()
            self.display_timer()
            self.display_tips()
            if self.remaining_time == 0 and self.winner == False:
                game_over_font = pygame.font.Font(None, 36)
                game_over_text = game_over_font.render("Perdu ! Vos données en cours de destruction...", True, (255, 0, 0))
                window_width, window_height = self.screen.get_size()
                text_width, text_height = game_over_text.get_size()
                text_x = (window_width - text_width) // 2
                text_y = (window_height - text_height) // 2
                self.screen.blit(game_over_text, (text_x, text_y))
                pygame.display.flip()
                pygame.time.wait(3000)
                running = False
            elif self.winner:
                winner_font = pygame.font.Font(None, 36)
                winner_text = winner_font.render("Bien joué ! Vos données sont en cours de récupération...", True, (0, 255, 0))
                window_width, window_height = self.screen.get_size()
                text_width, text_height = winner_text.get_size()
                text_x = (window_width - text_width) // 2
                text_y = (window_height - text_height) // 2
                self.screen.blit(winner_text, (text_x, text_y))
                pygame.display.flip()
                pygame.time.wait(3000)
                running = False

            if self.map_manager.active or self.input_box.active:
                self.color = self.color_active
            else:
                self.color = self.color_passive
            pygame.draw.rect(self.screen, self.color, self.input_rect, 2)
            pygame.display.flip()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if self.map_manager.active or self.input_box.active:
                        if event.key == pygame.K_BACKSPACE:
                            self.user_text = self.user_text[0:-1]
                        else:
                            self.user_text += event.unicode
                        self.handle_code_entry1()
                        self.handle_code_entry2()
                        self.handle_code_entry3()
                        self.handle_code_entry4()
                    if event.key == pygame.K_SPACE:
                        self.map_manager.check_npc_collisions(self.dialog_box)
                        if self.map_manager.current_map == "carte2":
                            self.level_2.check_lv2_collisions()
            self.input_box.handle_event()
            clock.tick(60)
        pygame.quit()