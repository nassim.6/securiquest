import pygame
from map import MapManager

class InputBox:
    def __init__(self):
        self.base_font = pygame.font.Font(None, 32)
        self.user_text = ''
        self.input_rect = pygame.Rect(200, 200, 140, 32)
        self.color_active = pygame.Color('lightskyblue3')
        self.color_passive = pygame.Color('gray15')
        self.color = self.color_passive
        self.active = False
        self.screen = pygame.display.set_mode((800, 600))

    def handle_event(self):
        self.text_surface = self.base_font.render(self.user_text, True, (255, 255, 255))
        self.screen.blit(self.text_surface, (self.input_rect.x + 5, self.input_rect.y + 5))
        self.input_rect.w = max(100, self.text_surface.get_width() + 10)
        if self.active:
            self.color = self.color_active
        else:
            self.color = self.color_passive
        pygame.draw.rect(self.screen, self.color, self.input_rect, 2)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_BACKSPACE:
                    self.user_text = self.user_text[0:-1]
                else:
                    self.user_text += event.unicode
