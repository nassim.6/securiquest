# SécuriQuest

#### Il s'agit d'un jeu dans le cadre des trophées NSI 2024. Ce fichier README fournit des instructions pour lancer et utiliser le jeu sur les systèmes Linux et/ou Windows. 

## Installation des Pré-requis (voir requirements.txt)
1. Vous pourrez installer Git depuis https://git-scm.com/. Une fois téléchargé, utilisez la commande ``git clone https://gitlab.com/nassim.6/securiquest.git`` dans le Git Bash.

2. Assurez-vous d'avoir Python installé sur votre système. Vous pouvez le télécharger depuis https://www.python.org/.

3. Installez les bibliothèques nécessaires en exécutant une par une les commandes suivante :
``pip install pygame``,
``pip install pytmx`` et
``pip install pyscroll``

## Contrôles du Jeu
- Utilisez les flèches du clavier pour vous déplacer dans le jeu.
- Appuyez sur la touche ESPACE pour interagir avec les objets et les personnages non jouables.

## Besoin d'Aide ?
Si vous rencontrez des problèmes lors de l'installation ou de l'utilisation du jeu, n'hésitez pas à nous contacter à nassimbatti@hotmail.com.

### Profitez bien du jeu !